/**
 * 
 */
package org.imbapp.domain;

import java.math.BigDecimal;

import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;

/**
 * @author gschlueter
 * 
 */
public class JodaMoneyConverter implements Converter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6265338415872161565L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.persistence.mappings.converters.Converter#
	 * convertObjectValueToDataValue(java.lang.Object,
	 * org.eclipse.persistence.sessions.Session)
	 */
	@Override
	public Object convertObjectValueToDataValue(Object objectValue,
			Session session) {
		if (objectValue == null) {
			return null;
		}

		BigMoney money = (BigMoney) objectValue;

		return money.getAmount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.persistence.mappings.converters.Converter#
	 * convertDataValueToObjectValue(java.lang.Object,
	 * org.eclipse.persistence.sessions.Session)
	 */
	@Override
	public Object convertDataValueToObjectValue(Object dataValue,
			Session session) {
		if (dataValue == null) {
			return null;
		}

		BigDecimal amount = (BigDecimal) dataValue;

		return BigMoney.of(CurrencyUnit.EUR, amount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.persistence.mappings.converters.Converter#isMutable()
	 */
	@Override
	public boolean isMutable() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.persistence.mappings.converters.Converter#initialize(org.
	 * eclipse.persistence.mappings.DatabaseMapping,
	 * org.eclipse.persistence.sessions.Session)
	 */
	@Override
	public void initialize(DatabaseMapping mapping, Session session) {
		mapping.getField().setType(BigDecimal.class);
	}

}
