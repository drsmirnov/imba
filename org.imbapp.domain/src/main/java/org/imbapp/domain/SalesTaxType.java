package org.imbapp.domain;

public enum SalesTaxType {

	NORMAL,
	DECREASED,
	NONE;
	
}
