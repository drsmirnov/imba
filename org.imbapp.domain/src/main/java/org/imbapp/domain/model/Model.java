package org.imbapp.domain.model;

public class Model<T> {

	T value;

	public Model(T value) {
		super();
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
	
}
