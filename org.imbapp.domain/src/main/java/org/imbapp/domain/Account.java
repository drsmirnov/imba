package org.imbapp.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.UuidGenerator;
import org.imbapp.domain.model.ModelBean;

@Entity
@UuidGenerator(name = "ACCOUNT_ID_GEN")
@Access(AccessType.FIELD)
public class Account extends ModelBean {

	@Id
	@GeneratedValue(generator = "ACCOUNT_ID_GEN")
	private String id;

	private String name;
	private AccountType accountType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		// firePropertyChange("id", this.id, this.id = id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		// this.name = name;
		firePropertyChange("name", this.name, this.name = name);
	}

	@Enumerated(EnumType.STRING)
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		// this.accountType = accountType;
		firePropertyChange("accountType", this.accountType, this.accountType = accountType);
	}

}
