package org.imbapp.domain;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.UuidGenerator;
import org.joda.money.BigMoney;

@Entity
@UuidGenerator(name = "PERSON_ID_GEN")
@Converter(name = "moneyConverter", converterClass = org.imbapp.domain.JodaMoneyConverter.class)
@Access(AccessType.FIELD)
public class Person implements Serializable {

	private static final long serialVersionUID = -6544867555562490322L;

	@Id
	@GeneratedValue(generator = "PERSON_ID_GEN")
	private String id;

	private String firstName;
	private String lastName;
	private int age;
	private BigMoney value;

	public Person() {
	}

	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Column(precision = 10, scale = 2)
	@Convert("moneyConverter")
	@Access(AccessType.PROPERTY)
	public BigMoney getValue() {
		return value;
	}

	public void setValue(BigMoney value) {
		this.value = value;
	}

}
