package org.imbapp.domain;

public enum AccountType {

	CASH,
	BANK,
	COST,
	INCOME,
	SPECIAL;
	
}
