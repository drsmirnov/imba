package org.imbapp.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.UuidGenerator;
import org.imbapp.domain.model.ModelBean;

@Entity
@UuidGenerator(name = "CREDITOR_ID_GEN")
@Access(AccessType.FIELD)
public class Creditor extends ModelBean {

	@Id
	@GeneratedValue(generator = "CREDITOR_ID_GEN")
	private String id;

	private String name;
	private int uiOrder;
	private InvoiceType preferedInvoiceType;
	private SalesTaxType preferedSalesTaxType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		// firePropertyChange("id", this.id, this.id = id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		// this.name = name;
		firePropertyChange("name", this.name, this.name = name);
	}

	public int getUiOrder() {
		return uiOrder;
	}

	public void setUiOrder(int uiOrder) {
		// this.uiOrder = uiOrder;
		firePropertyChange("uiOrder", this.uiOrder, this.uiOrder = uiOrder);
	}

	@Enumerated(EnumType.STRING)
	public InvoiceType getPreferedInvoiceType() {
		return preferedInvoiceType;
	}

	public void setPreferedInvoiceType(InvoiceType preferedInvoiceType) {
		// this.preferedInvoiceType = preferedInvoiceType;
		firePropertyChange("preferedInvoiceType", this.preferedInvoiceType,
				this.preferedInvoiceType = preferedInvoiceType);
	}

	@Enumerated(EnumType.STRING)
	public SalesTaxType getPreferedSalesTaxType() {
		return preferedSalesTaxType;
	}

	public void setPreferedSalesTaxType(SalesTaxType preferedSalesTaxType) {
		// this.preferedSalesTaxType = preferedSalesTaxType;
		firePropertyChange("preferedSalesTaxType", this.preferedSalesTaxType,
				this.preferedSalesTaxType = preferedSalesTaxType);
	}

}
