package org.imbapp.helper;

public class EnumUtils {

	private EnumUtils() {
	}

	@SuppressWarnings({ "rawtypes" })
	public static <T extends Enum> String[] getEnumValuesAsStringArray(Class<T> enumclass) {
		String[] values = new String[enumclass.getEnumConstants().length];
		int i = 0;
		for (Enum enumeration : enumclass.getEnumConstants()) {
			values[i++] = enumeration.name();
		}
		return values;
	}
}
