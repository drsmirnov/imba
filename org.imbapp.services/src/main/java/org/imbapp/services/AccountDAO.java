package org.imbapp.services;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.imbapp.domain.Account;
import org.imbapp.domain.QAccount;

import com.mysema.query.types.EntityPath;

@Creatable
public class AccountDAO extends AbstractDAO<Account> {

	@Override
	protected EntityPath<Account> getMetaModel() {
		return QAccount.account;
	}

}
