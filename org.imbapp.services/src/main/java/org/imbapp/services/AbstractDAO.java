package org.imbapp.services;

import java.util.List;

import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.eclipse.gemini.ext.di.GeminiPersistenceContext;

import com.mysema.query.jpa.EclipseLinkTemplates;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.EntityPath;

public abstract class AbstractDAO<T> {

	@Inject
	@GeminiPersistenceContext(unitName = "configured")
	private EntityManager em;

	public AbstractDAO() {
		super();
	}
	
	abstract protected EntityPath<T> getMetaModel();

	protected JPQLQuery createQuery() {
		JPQLQuery query = new JPAQuery(em, EclipseLinkTemplates.DEFAULT);
		return query;
	}
	
	protected JPQLQuery createFromQuery() {
		return createQuery().from(getMetaModel());
	}

	public void save(T dataObj) {
		EntityTransaction trx = em.getTransaction();
		trx.begin();
		em.merge(dataObj);
		trx.commit();
	}

	public T load(String id) {
		return em.find(getMetaModel().getType(), id);
	}

	@PreDestroy
	public void destroy() {
		if (em != null && em.isOpen()) {
			em.close();
		}
	}

	public List<T> findAll() {
		return createFromQuery().list(getMetaModel());
	}

	public void delete(T object) {
		if(object != null){
			EntityTransaction trx = em.getTransaction();
			trx.begin();
			em.remove(em.merge(object));
			trx.commit();
		}
	}

}