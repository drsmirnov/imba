package org.imbapp.services;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.imbapp.domain.Person;
import org.imbapp.domain.QPerson;

import com.mysema.query.types.EntityPath;

@Creatable
public class PersonDAO extends AbstractDAO<Person> {

	@Override
	protected EntityPath<Person> getMetaModel() {
		return QPerson.person;
	}

}
