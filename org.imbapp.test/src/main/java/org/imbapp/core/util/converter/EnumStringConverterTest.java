package org.imbapp.core.util.converter;

import static org.junit.Assert.*;

import org.imbapp.helper.TestEnum;
import org.junit.Test;

public class EnumStringConverterTest {

	@Test
	public void testMapEnum() {
		EnumStringConverter converter = new EnumStringConverter();
		assertEquals("PAAR",converter.convert(TestEnum.PAAR));
	}

	@Test
	public void testMapNull() {
		EnumStringConverter converter = new EnumStringConverter();
		Object convert = converter.convert(null);
		assertNull(convert);
	}
	
	@Test
	public void testMapString() {
		EnumStringConverter converter = new EnumStringConverter(TestEnum.class);
		
		assertEquals(TestEnum.PAAR,converter.convert("PAAR"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testMapInvalid() {
		EnumStringConverter converter = new EnumStringConverter();
		
		assertEquals(TestEnum.PAAR,converter.convert("PAAR"));
	}

}
