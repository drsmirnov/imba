package org.imbapp.domain.model;

import static org.junit.Assert.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.junit.Test;

public class ModelBeanTest {

	@Test
	public void testFirePropertyChanged() {
		TestModel model = new TestModel();
		model.setName("alt");
		model.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals("name") && evt.getNewValue().equals("neu") && evt.getOldValue().equals("alt")){
					((TestModel)evt.getSource()).setName("geaendert");
				}
			}
		});
		model.setName("neu");
		assertEquals("Es hätte ein PopertyChangeEvent geworfen werden müssen!", "geaendert",model.getName());
	}
	
}
