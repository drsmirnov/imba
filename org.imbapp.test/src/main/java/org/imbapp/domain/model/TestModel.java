package org.imbapp.domain.model;

import java.util.Date;

public class TestModel extends ModelBean {

	private String name;
	private String ort;
	private Date datum;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		firePropertyChange("name", this.name, this.name = name);
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		firePropertyChange("datum", this.datum, this.datum = datum);
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
//		this.ort = ort;
		firePropertyChange("ort", this.ort, this.ort = ort);
	}
	
	

}
