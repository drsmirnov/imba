package org.imbapp.helper;

import static org.junit.Assert.*;

import org.junit.Test;

public class EnumUtilsTest {

	@Test
	public void testGetEnumValuesAsStringArray() {
		String[] values = EnumUtils.getEnumValuesAsStringArray(TestEnum.class);
		assertEquals(4, values.length);
		assertEquals("EIN", values[1]);
	}

}
