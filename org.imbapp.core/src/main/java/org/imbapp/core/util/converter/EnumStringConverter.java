package org.imbapp.core.util.converter;

import org.eclipse.core.databinding.conversion.Converter;

public class EnumStringConverter extends Converter {

	public EnumStringConverter(){
		super(null, null);
	}
			
	public EnumStringConverter(Object fromType, Object toType) {
		super(fromType, toType);
	}

	@SuppressWarnings("rawtypes")
	public <T extends Enum> EnumStringConverter(Class<T> toType) {
		super(null, toType);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object convert(Object fromObject) {
		if(fromObject==null) {
			return null;
		}
		if(Enum.class.isAssignableFrom(fromObject.getClass())){
			return ((Enum<?>)fromObject).name();
		}
		if(getToType()!=null && Enum.class.isAssignableFrom((Class<?>) getToType())){
			return Enum.<Enum>valueOf((Class<Enum>)getToType(),String.valueOf(fromObject));
		}
		throw new IllegalArgumentException("Die Eingabe kann nicht verarbeitet werden");
	}

}
