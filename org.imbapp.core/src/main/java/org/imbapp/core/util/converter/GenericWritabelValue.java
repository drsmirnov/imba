package org.imbapp.core.util.converter;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.imbapp.domain.model.ModelBean;

public class GenericWritabelValue<T extends ModelBean> extends WritableValue {

	public GenericWritabelValue() {
		super();
	}

	/**
	 * Übergebener Wert darf nicht null sein!
	 * 
	 * @param initialValue
	 */
	public GenericWritabelValue(Object initialValue) {
		super(initialValue,initialValue.getClass());
	}

	public GenericWritabelValue(Realm realm, Object initialValue, Object valueType) {
		super(realm, initialValue, valueType);
	}

	public GenericWritabelValue(Realm realm) {
		super(realm);
	}

	@SuppressWarnings("unchecked")
	public T getTypedValue() {
		return (T) getValue();
	}

	public void setTypedValue(T value){
		setValue(value);
	}
}
