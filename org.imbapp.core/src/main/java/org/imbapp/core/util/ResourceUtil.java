package org.imbapp.core.util;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class ResourceUtil {

	private ResourceUtil() {
	}
	
	  /**
	   * This Method create an Image for an image in icon Folder of this bundle
	   * @param file
	   * @return
	   * @throws RuntimeException if the icon folder doesn't contain the requested icon
	   */
	  public static Image getImage(String file) {
	    Bundle bundle = FrameworkUtil.getBundle(ResourceUtil.class);
	    URL url = FileLocator.find(bundle, new Path("icons/" + file), null);
	    if(url==null){
	    	throw new RuntimeException(String.format("The image %s doesn't exists in the icons folder",file));
	    }
	    ImageDescriptor image = ImageDescriptor.createFromURL(url);
	    return image.createImage();
	  } 
	
}
