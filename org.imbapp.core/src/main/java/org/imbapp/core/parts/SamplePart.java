/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.imbapp.core.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.imbapp.domain.Person;
import org.imbapp.services.PersonDAO;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;
import org.eclipse.swt.widgets.Text;

public class SamplePart {

	private Label label;
	private TableViewer tableViewer;
	private Text text;

	@Inject
	PersonDAO personDAO;

	@PostConstruct
	public void createComposite(Composite parent) {
		parent.setLayout(new GridLayout());

		Person person = new Person("Gerrit", "Schlüter");
		person.setValue(BigMoney.of(CurrencyUnit.EUR, 123456.99));
		
		personDAO.save(person);
		
		String id = person.getId();

		label = new Label(parent, SWT.NONE);
		label.setText("Sample table");
		
		text = new Text(parent, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		tableViewer = new TableViewer(parent);
		Person loadedPerson = personDAO.load(id);
		
		tableViewer.add(loadedPerson.getFirstName());
		tableViewer.add(loadedPerson.getValue().toString());
		tableViewer.add(id);
		tableViewer.add(StringUtils.capitalize("abc"));
		tableViewer.add("Sample item 4");
		tableViewer.add("Sample item 5");
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	@Focus
	public void setFocus() {
		// tableViewer.getTable().setFocus();
	}
}
