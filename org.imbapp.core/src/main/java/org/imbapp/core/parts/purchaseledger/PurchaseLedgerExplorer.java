package org.imbapp.core.parts.purchaseledger;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.swt.widgets.Composite;
import org.imbapp.core.ui.explorer.date.AnnualFile;
import org.imbapp.core.ui.explorer.date.DateTreeView;
import org.imbapp.core.ui.explorer.date.MonthlyFile;

public class PurchaseLedgerExplorer {

	protected DateTreeView treeViewer;
	
	@PostConstruct
	public void createComposite(Composite parent) {
		treeViewer = new DateTreeView(parent);
		treeViewer.setInput(getAnnualFilesList());
	}

	protected List<AnnualFile> getAnnualFilesList() {
		List<AnnualFile> files = new ArrayList<>();
		AnnualFile file2012 = new AnnualFile(2012);
		file2012.getMonthlyFiles().add(new MonthlyFile(2, 103));
		file2012.getMonthlyFiles().add(new MonthlyFile(4, 88));
		file2012.getMonthlyFiles().add(new MonthlyFile(10, 94));
		files.add(file2012);
		files.add(new AnnualFile(2013));
		return files;
	}
	
}
