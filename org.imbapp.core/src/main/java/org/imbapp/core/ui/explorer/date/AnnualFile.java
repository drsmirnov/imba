package org.imbapp.core.ui.explorer.date;

import java.util.ArrayList;
import java.util.List;

public class AnnualFile {

	private List<MonthlyFile> monthlyFiles;
	private int year;

	public AnnualFile(int year) {
		super();
		monthlyFiles = new ArrayList<>(12);
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public List<MonthlyFile> getMonthlyFiles() {
		return monthlyFiles;
	}

	public void setMonthlyFiles(List<MonthlyFile> monthlyFiles) {
		this.monthlyFiles = monthlyFiles;
	}

}