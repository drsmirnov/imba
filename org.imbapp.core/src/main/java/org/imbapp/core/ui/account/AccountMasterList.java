package org.imbapp.core.ui.account;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.imbapp.domain.Account;
import org.imbapp.services.AccountDAO;

public class AccountMasterList {

	public static final String TOPIC_ACCOUNT_SELECTED = "TOPIC_ACCOUNT_SELECTED"; 
	
	private Table table;
	private TableViewer viewer; 

	@Inject
	private IEventBroker eventBroker;

	
	@PostConstruct
	public void createComposite(final Composite parent) {
		parent.setLayout(new FillLayout(SWT.HORIZONTAL));

		viewer = new TableViewer(parent, SWT.SINGLE | SWT.H_SCROLL
		      | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		createColumns(viewer);

		table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			  @Override
			  public void selectionChanged(SelectionChangedEvent event) {
			    IStructuredSelection selection = (IStructuredSelection)
			        viewer.getSelection();
			    eventBroker.send(TOPIC_ACCOUNT_SELECTED,selection.getFirstElement());
			  }
			}); 
		
		updateAccountList(null);
	}
	
	@Inject
	AccountDAO accountDAO;
	
	@Inject
	@Optional
	private void updateAccountList(@UIEventTopic(AccountDetailForm.TOPIC_ACCOUNT_STORED) Account account) {
//		if(account!=null){
		viewer.setInput(accountDAO.findAll());
//		}
	}

	private void createColumns(TableViewer viewer) {
		// create a column for the first name
		TableViewerColumn colName = new TableViewerColumn(viewer, SWT.NONE);
		colName.getColumn().setWidth(200);
		colName.getColumn().setText("Name");
		colName.setLabelProvider(new ColumnLabelProvider() {
		  @Override
		  public String getText(Object element) {
		    Account a = (Account) element;
		    return a.getName();
		  }
		});
		
		TableViewerColumn colAccountType = new TableViewerColumn(viewer, SWT.NONE);
		colAccountType.getColumn().setWidth(400);
		colAccountType.getColumn().setText("Kontotype");
		colAccountType.setLabelProvider(new ColumnLabelProvider() {
		  @Override
		  public String getText(Object element) {
		    Account a = (Account) element;
		    return WordUtils.capitalize(StringUtils.lowerCase(String.valueOf(a.getAccountType())));
		  }
		});

	}

}
