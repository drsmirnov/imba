package org.imbapp.core.ui.explorer.date;

import java.text.DateFormatSymbols;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.imbapp.core.util.ResourceUtil;

public class ExplorerLabelProvider implements ILabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof AnnualFile) {
			return ResourceUtil.getImage("folder.png");
		} else if (element instanceof MonthlyFile) {
			return ResourceUtil.getImage("folder_table.png");
		}
		throw new RuntimeException(String.format(Messages.ExplorerLabelProvider_error,element.getClass()));
	}

	@Override
	public String getText(Object element) {
		if (element instanceof AnnualFile) {
			return String.valueOf(((AnnualFile) element).getYear());
		} else if (element instanceof MonthlyFile) {
			MonthlyFile monthlyFile = (MonthlyFile)element;
			return String.format(Messages.ExplorerLabelProvider_dateFormat,new DateFormatSymbols().getMonths()[monthlyFile.getMonth() - 1],monthlyFile.getCount());
		}
		throw new RuntimeException(String.format(Messages.ExplorerLabelProvider_error,element.getClass()));
	}

}
