package org.imbapp.core.ui.explorer.date;


public class MonthlyFile {

	private int month;
	private int count;

	public MonthlyFile(int month) {
		super();
		this.month = month;
	}

	public MonthlyFile(int month, int count) {
		super();
		this.month = month;
		this.count = count;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}