package org.imbapp.core.ui.explorer.date;

import java.util.List;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

public class DateTreeView extends TreeViewer{

	public DateTreeView(Composite parent, int style) {
		super(parent, style);
		setupTreeview();
	}

	public DateTreeView(Composite parent) {
		super(parent);
		setupTreeview();
	}

	public DateTreeView(Tree tree) {
		super(tree);
		setupTreeview();
	}
	
	protected void setupTreeview(){
		setContentProvider(createContentProvider());
		setLabelProvider(createLabelProvider());
	}

	protected ExplorerLabelProvider createLabelProvider() {
		return new ExplorerLabelProvider();
	}

	protected ExplorerContentProvider createContentProvider() {
		return new ExplorerContentProvider();
	}

	public void setInput(List<AnnualFile> files){
		super.setInput(files);
	}
	
}
