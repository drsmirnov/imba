package org.imbapp.core.ui.account;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.imbapp.core.handlers.DeleteHandler;
import org.imbapp.core.handlers.DeleteHandler.DeleteHandlerPaylod;
import org.imbapp.core.handlers.NewHandler;
import org.imbapp.core.handlers.NewHandler.NewHandlerPaylod;
import org.imbapp.core.util.converter.EnumStringConverter;
import org.imbapp.core.util.converter.GenericWritabelValue;
import org.imbapp.domain.Account;
import org.imbapp.domain.AccountType;
import org.imbapp.helper.EnumUtils;
import org.imbapp.services.AccountDAO;

public class AccountDetailForm {

	public static final String TOPIC_ACCOUNT_STORED = "TOPIC_ACCOUNT_STORED"; 
	
	@Inject
	MDirtyable dirty;
	
	@Inject
	AccountDAO accountDAO;
	
	private DataBindingContext m_bindingContext;

	private Text text;
	private ComboViewer combo;
	GenericWritabelValue<Account> accountModel = new GenericWritabelValue<>(new Account());

	@PostConstruct
	public void createComposite(Composite parent) {
		parent.setLayout(new GridLayout(2, false));

		Label lblName = new Label(parent, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name");

		text = new Text(parent, SWT.BORDER);
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_text.widthHint = 200;
		text.setLayoutData(gd_text);

		Label lblKontotyp = new Label(parent, SWT.NONE);
		lblKontotyp.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblKontotyp.setText("Kontotyp");

		combo = new ComboViewer(parent, SWT.NONE);
		GridData gd_combo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_combo.widthHint = 200;

		ArrayContentProvider cp = new ArrayContentProvider();
		combo.setContentProvider(cp);
		combo.setInput(EnumUtils.getEnumValuesAsStringArray(AccountType.class));

		m_bindingContext = initDataBindings();

		combo.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				dirty.setDirty(true);
			}
			
		});
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				dirty.setDirty(true);
			}
		});
	}

	@Inject
	@Optional
	private void showAccount(@UIEventTopic(AccountMasterList.TOPIC_ACCOUNT_SELECTED) Account account,Shell shell) {
		if(dirty.isDirty()){
			if(!MessageDialog.openConfirm(shell, "Ungespeicherte Änderungen", "Das Formular enthält ungespeicherte Änderungen. Wollen Sie diese verwerfen?")){
				return;
			};
		}
		accountModel.setValue(account);
		dirty.setDirty(false);
	}
	
	protected String getPartId(){
		return "org.imbapp.core.ui.parts.account.accountdetailform";
	}
	
	@Inject
	@Optional
	private void createNewAccount(@UIEventTopic(NewHandler.TOPIC_CREATENEW) NewHandlerPaylod payload,Shell shell,EPartService partService) {
		if(!getPartId().equals(partService.getActivePart().getElementId())){
			return;
		}
		if(dirty.isDirty()){
			if(!MessageDialog.openConfirm(shell, "Ungespeicherte Änderungen", "Das Formular enthält ungespeicherte Änderungen. Wollen Sie diese verwerfen?")){
				return;
			};
		}
		accountModel.setValue(new Account());
		m_bindingContext.updateTargets();
		dirty.setDirty(false);
	}
	
	@Inject
	@Optional
	private void deleteAccount(@UIEventTopic(DeleteHandler.TOPIC_DELETE) DeleteHandlerPaylod payload,Shell shell,EPartService partService,IEventBroker eventBroker) {
		if(!getPartId().equals(partService.getActivePart().getElementId())){
			return;
		}
		if(!MessageDialog.openConfirm(shell, "Löschbestätigung", "Wollen Sie den ausgewählten Datensatz wirklich löschen?")){
				return;
		};
		accountDAO.delete(accountModel.getTypedValue());
		accountModel.setValue(new Account());
		m_bindingContext.updateTargets();
		dirty.setDirty(false);
		eventBroker.send(AccountDetailForm.TOPIC_ACCOUNT_STORED,accountModel.getTypedValue());
	}
	
	@Persist
	public void saveAccount(MDirtyable dirty,IEventBroker eventBroker) {
  	  m_bindingContext.updateModels();
	  Account account = accountModel.getTypedValue();
	  accountDAO.save(account);
	  dirty.setDirty(false);
	  eventBroker.send(AccountDetailForm.TOPIC_ACCOUNT_STORED,account);
	} 
	
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeTextTextObserveWidget = WidgetProperties.text(SWT.FocusOut).observe(text);
		IObservableValue accountModelNameObserveDetailValue = BeanProperties.value(Account.class, "name", String.class).observeDetail(accountModel);
		bindingContext.bindValue(observeTextTextObserveWidget, accountModelNameObserveDetailValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_ON_REQUEST), null);
		//
		IObservableValue observeSingleSelectionCombo = ViewerProperties.singleSelection().observe(combo);
		IObservableValue accountModelAccountTypeObserveDetailValue = BeanProperties.value(Account.class, "accountType", AccountType.class).observeDetail(accountModel);
		UpdateValueStrategy strategy = new UpdateValueStrategy(UpdateValueStrategy.POLICY_ON_REQUEST);
		strategy.setConverter(new EnumStringConverter(AccountType.class));
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new EnumStringConverter(AccountType.class));
		bindingContext.bindValue(observeSingleSelectionCombo, accountModelAccountTypeObserveDetailValue, strategy, strategy_1);
		//
		return bindingContext;
	}

}
