package org.imbapp.core.ui.explorer.date;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ExplorerContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if(parentElement instanceof List<?>){
			return ((List<?>)parentElement).toArray();
		}
		if(parentElement instanceof AnnualFile){
			return ((AnnualFile)parentElement).getMonthlyFiles().toArray();
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		throw new UnsupportedOperationException("This method has to be implemented");
	}

	@Override
	public boolean hasChildren(Object element) {
		 return getChildren(element).length > 0;
	}
	
}
