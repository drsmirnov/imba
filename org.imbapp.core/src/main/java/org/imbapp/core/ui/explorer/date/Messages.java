package org.imbapp.core.ui.explorer.date;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.imbapp.core.parts.purchaseledger.explorer.messages"; //$NON-NLS-1$
	public static String ExplorerLabelProvider_dateFormat;
	public static String ExplorerLabelProvider_error;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
