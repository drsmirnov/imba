package org.imbapp.core.perspectives;

public enum PerspectiveIds {

	PURCHASE_LEDGER("org.imbapp.core.perspective.purchaseLedger"),
	CASHBOOK("org.imbapp.core.perspective.cashBook"),
	ADMINISTRATION("org.imbapp.core.perspective.administration");
	
	private final String id;
	
	private PerspectiveIds(String id) {
		this.id=id;
	}

	public String getId() {
		return id;
	}
	
}
