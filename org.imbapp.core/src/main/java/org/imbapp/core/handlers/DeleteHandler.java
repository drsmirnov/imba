 
package org.imbapp.core.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

public class DeleteHandler {
	
	public static final String TOPIC_DELETE = "TOPIC_DELETE";
	
	@Execute
	public void execute(IEventBroker eventBroker,EPartService partService)
			throws InvocationTargetException, InterruptedException {
		eventBroker.send(
				DeleteHandler.TOPIC_DELETE, new DeleteHandlerPaylod());
	}
	
	public class DeleteHandlerPaylod{
		
	}
}