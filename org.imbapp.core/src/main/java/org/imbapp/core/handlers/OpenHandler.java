/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.imbapp.core.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

public class OpenHandler {

	@Execute
	public void execute(
			//@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			EPartService partService, 
			EModelService modelService,
			MApplication app) {

		String elementId = modelService.getPerspectiveFor(
				partService.getActivePart()).getElementId();

		if ("purchaseLedgerPerspective".equals(elementId)) {
			MPerspective element = (MPerspective) modelService.find(
					"cashBookPerspective", app);
			partService.switchPerspective(element);
		} else {
			MPerspective element = (MPerspective) modelService.find(
					"purchaseLedgerPerspective", app);
			partService.switchPerspective(element);
		}

		// FileDialog dialog = new FileDialog(shell);
		// dialog.open();
	}
}
