 
package org.imbapp.core.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

public class NewHandler {
	
	public static final String TOPIC_CREATENEW = "TOPIC_CREATENEW";
	
	@Execute
	public void execute(IEventBroker eventBroker,EPartService partService)
			throws InvocationTargetException, InterruptedException {
		eventBroker.send(
				NewHandler.TOPIC_CREATENEW, new NewHandlerPaylod());
	}
	
	public class NewHandlerPaylod{
		
	}
		
}