 
package org.imbapp.core.handlers.navigation.perspective;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.imbapp.core.perspectives.PerspectiveIds;

public class SwitchToCashBookPerspectiveHandler {
	
	@Execute
	public void execute(EPartService partService, EModelService modelService,MApplication app) {
		MPerspective element = (MPerspective) modelService.find(
				PerspectiveIds.CASHBOOK.getId(), app);
		partService.switchPerspective(element);
	}
	
	
	@CanExecute
	public boolean canExecute(EPartService partService, EModelService modelService) {
		String elementId = modelService.getPerspectiveFor(
				partService.getActivePart()).getElementId();
		return !PerspectiveIds.CASHBOOK.getId().equals(elementId);
	}
		
}