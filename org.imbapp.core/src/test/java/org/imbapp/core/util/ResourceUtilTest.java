package org.imbapp.core.util;

import org.imbapp.core.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Test;

public class ResourceUtilTest {

	@Test
	public void testGetImage() {
		Assert.assertNotNull(ResourceUtil.getImage("add.png"));
	}
	
	@Test(expected=RuntimeException.class)
	public void testGetNoneExistingImage() {
		ResourceUtil.getImage("someNoneExistingImage.png");
	}

}
