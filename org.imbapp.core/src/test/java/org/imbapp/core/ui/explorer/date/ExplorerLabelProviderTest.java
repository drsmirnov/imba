package org.imbapp.core.ui.explorer.date;

import org.imbapp.core.ui.explorer.date.AnnualFile;
import org.imbapp.core.ui.explorer.date.ExplorerLabelProvider;
import org.imbapp.core.ui.explorer.date.MonthlyFile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExplorerLabelProviderTest {

	ExplorerLabelProvider labelProvider;
	
	@Before
	public void setupTest(){
		labelProvider = new ExplorerLabelProvider();
	}
	
	@Test
	public void testResolveYear() {
		
		AnnualFile annualFile = new AnnualFile(2010);
		Assert.assertEquals("2010", labelProvider.getText(annualFile));

	}

	@Test
	public void testResolveMonth() {
		
		MonthlyFile monthlyFile = new MonthlyFile(2, 14);
		Assert.assertEquals("Februar (14)", labelProvider.getText(monthlyFile));

	}
	
	@Test
	public void testResolveMonthWithoutCount() {
		
		MonthlyFile monthlyFile = new MonthlyFile(10);
		Assert.assertEquals("Oktober (0)", labelProvider.getText(monthlyFile));

	}
	
	@Test(expected=RuntimeException.class)
	public void testResolveUnknownClass() {
		
		labelProvider.getText("someString");

	}
	
}
